import math
from random import random

# main game loop - initialize all of the values we will need; get the
# range for the game; prompt the user to think of a number in the range and
# then make guesses on the number until we get the correct number. and then
# gloat about how smart you are...
def play_game(high_limit, total_guesses, all_guesses):
  (low_limit, high_limit, guess, result) = setup()
  print(f"now think of a number between {low_limit} and {high_limit}")
  guesses = 0
  while result != 'c' and guess != None:
    guesses += 1
    (guess, low_limit, high_limit) = new_guess(guess, result, low_limit, high_limit, all_guesses )
    if guess == None:
      break
    result = respond_to_guess(guess)

  if guess:
    print(f"{guess}!!! yay me!!!")

  print (f"it took me {guesses} guesses")
  total_guesses += guesses
  return total_guesses

# initialize the low_limit, high_limit, current guess and initial result
# current guess always starts out as the high_limit; 
# initial result is always 'h' so that we start by guessing below the high_limit
def setup():
  low_limit = global_low_limit
  high_limit = global_high_limit
  guess = high_limit
  result = 'h'
  return (low_limit, high_limit, guess, result)

# use the the result ('h' or 'l') that we got on our last guess
# and adjust the next guess by finding the midpoint between the current low_limit
# and high_limit and subtracting that from the previous guess (if we were too high)
# or adding it to the previous guess (if we were too low). if we were too high, set the
# high_limit to our previous guess; if we were too low, set the low limit to our previous
# guess.
def new_guess(last_guess, result, low_limit, high_limit, all_guesses ):
  if result == 'h':
    high_limit = last_guess
    guess = high_limit - math.floor((high_limit - low_limit) / 2)
  else:
    low_limit = last_guess
    guess = low_limit + math.ceil((high_limit - low_limit) / 2)
  
  diff = high_limit - low_limit
  if high_limit - low_limit <= 2:
    if guess in all_guesses:
      print(f"not cool. i already guessed {guess}")
    else:
      print(f"okay. your number is {guess}")
    return (None, low_limit, high_limit)

  all_guesses.append(guess)
  return (guess, low_limit, high_limit)

# prompt the user for a 'l' (too low), 'h' (too high) or 'c' (correct) response to our guess 
def respond_to_guess(guess):
  valid_responses = ['l', 'h', 'c']
  response = ''
  while response not in valid_responses:
    print(f"is your number {guess}? [c = correct, l = too low, h = too high]")
    response = input()  
  return response

# prompt the user to play again
def prompt_play_again():
  print('')
  print('play_again (y/n)?')
  play_again = input()
  return play_again

# set the range to be used by our set of games
def prompt_for_upper_limit():
  print ("Please enter an upper limit for our game:")
  global_high_limit = int(input())
  return global_high_limit

# print the rules
def do_rules():
  print("""
number_guessr --
you get to pick a range for the game (1 .. n), then you imagine a number in that range
i then try to guess your imagined number. with each guess, you tell me if my guess is too high (h)
too low (l) or correct (c). i will then use your feedback to hopefully zero in on your 
imagined number.
""")

# output aggregate stats for the games we played
def do_output(games_count, total_guesses):
  avg_guesses = '{0:.3g}'.format(total_guesses/games_count)
  print(f"i averaged {str(avg_guesses)} guesses per game for {str(games_count)} game(s) ")

##################################################################
# main
play_again = 'y'
games_count = 0
total_guesses = 0
global_high_limit = 0
global_low_limit = 0
all_guesses = []

do_rules()
while play_again == 'y':
  if global_high_limit == 0:
    global_high_limit = prompt_for_upper_limit()
  games_count += 1
  total_guesses = play_game(global_high_limit, total_guesses, all_guesses)
  do_output(games_count, total_guesses)
  play_again = prompt_play_again()

print('bye')
