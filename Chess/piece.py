from board import Board

class Piece():
  def __init__(self, location):
    self.location = location 
    self.next_moves = []
    self.allowed_positions = Board.allowed_positions() 

  def __str__(self):
    return str(self.name) + ' ' + str(self.location)

  # return the name of the piece
  def get_name(self):
    return self.name

  # return the current location of the piece (x,y) 
  def get_location(self):
    return self.location

  # return the stringified list of next moves for the piece
  def get_next_moves(self):
    return str(self.next_moves)

  # calculate the set of diagonal moves available for the piece
  def calculate_diagonal_moves(self):
    (loc_x, loc_y) = list(self.location)
    for r in range(-7, 7):
      if r == 0:
        continue

      # x direction is positive; y direction is positive
      new_loc_x = chr(ord(loc_x) + r)
      new_loc_y = str(int(loc_y) + r)
      if new_loc_x + new_loc_y in self.allowed_positions:
        self.next_moves.append(new_loc_x + new_loc_y)
      
      # x direction is negative; y direction is positive
      new_loc_x = chr(ord(loc_x) - r)
      new_loc_y = str(int(loc_y) + r)
      if new_loc_x + new_loc_y in self.allowed_positions:
        self.next_moves.append(new_loc_x + new_loc_y)

      # x direction is positive; y direction is negative
      new_loc_x = chr(ord(loc_x) + r)
      new_loc_y = str(int(loc_y) - r)
      if new_loc_x + new_loc_y in self.allowed_positions:
        self.next_moves.append(new_loc_x + new_loc_y)

      # x direction is negative; y direction is negative
      new_loc_x = chr(ord(loc_x) - r)
      new_loc_y = str(int(loc_y) - r)
      if new_loc_x + new_loc_y in self.allowed_positions:
        self.next_moves.append(new_loc_x + new_loc_y)

  # calculate the set of horizontal moves available for the piece
  def calculate_horizontal_moves(self):
    (loc_x, loc_y) = list(self.location)
    for r in range(-7, 7):
      if r == 0:
        continue
      new_loc_x = chr(ord(loc_x) + r)
      new_loc_y = loc_y
      if new_loc_x + new_loc_y in self.allowed_positions:
        self.next_moves.append(new_loc_x + new_loc_y)

  # calculate the set of vertical moves available for the piece
  def calculate_vertical_moves(self):
    (loc_x, loc_y) = list(self.location)
    for r in range(-7, 7):
      if r == 0:
        continue
      new_loc_x = loc_x
      new_loc_y = str(int(loc_y) + r)
      if new_loc_x + new_loc_y in self.allowed_positions:
        self.next_moves.append(new_loc_x + new_loc_y)

  # calculate the set of moves available for a piece that has limited moves - king, pawn, knight
  def calculate_spec_moves(self, move):
    (loc_x, loc_y) = list(self.location)
    (x_dist, y_dist) = move
    new_loc_x = chr(ord(loc_x) + int(x_dist))
    new_loc_y = str(int(loc_y) + int(y_dist))
    if new_loc_x + new_loc_y in self.allowed_positions:
      self.next_moves.append(new_loc_x + new_loc_y)

  # determine what constraints are imposed on the piece and get the set of diagonal, horizontal
  # and vertical and specific moves that the piece can make    
  def calculate_next_moves(self):
    print(self.location)
    next_moves = []
    for move in self.can_move:
      horiz = move[0]
      vert = move[1]
      if horiz == '*' and vert == '*':
        self.calculate_diagonal_moves()
      elif horiz == '*':
        self.calculate_horizontal_moves()
      elif vert == '*':
        self.calculate_vertical_moves()
      else:
        self.calculate_spec_moves(move)

    self.next_moves = list(set(self.next_moves))
    return self.next_moves

  def move(self, destination):
    origin = self.location
    if destination in self.next_moves:
      self.location = destination
      print(f"new location for {self.get_name()} at {origin} is {self.location}")
      return True
    else:
      print(f"moving {self.get_name()} at {origin} to {destination} is not permitted")
      return False

# knight
class Knight(Piece):
  def __init__(self, location):
    super().__init__(location)
    self.name = 'Knight'
    self.can_move = [
      ['1','2'], 
      ['-1','-2'], 
      ['1','-2'], 
      ['-1','2'],
      ['2','1'], 
      ['-2','-1'], 
      ['2','-1'], 
      ['-2','1']
    ]

# queen
class Queen(Piece):
  def __init__(self, location):
    super().__init__(location)
    self.name = 'Queen'
    self.can_move =[
      ['0','*'],
      ['*', '0'], 
      ['*', '*'] 
    ]

# pawn
class Pawn(Piece):
  def __init__(self, location):
    super().__init__(location)
    self.name = 'Pawn'
    self.can_move =[
      ['1', '0']
    ]

# king
class King(Piece):
  def __init__(self, location):
    super().__init__(location)
    self.name = 'King'
    self.can_move =[
      ['1', '0'],
      ['1', '1'],
      ['1', '-1'],
      ['0', '1'],
      ['0', '-1'],
      ['-1', '0'],
      ['-1', '1'],
      ['-1', '-1']
    ]

# bishop
class Bishop(Piece):
  def __init__(self, location):
    super().__init__(location)
    self.name = 'Bishop'
    self.can_move =[
      ['*', '*']
    ]

# rook
class Rook(Piece):
  def __init__(self, location):
    super().__init__(location)
    self.name = 'Rook'
    self.can_move =[ 
      ['0','*'], 
      ['*','0']
    ]
