import unittest
from board import Board

class TestBoard(unittest.TestCase):
  def setUp(self):
    self.board = Board()

  def test_locations(self):
    # both indexes out of range
    self.assertNotIn('z9', Board.allowed_positions())
    # vertical index out of range
    self.assertNotIn('a0', Board.allowed_positions())
    # horizontal index out of range
    self.assertNotIn('j1', Board.allowed_positions())
    # in range
    self.assertIn('a1', Board.allowed_positions())
    self.assertIn('b4', Board.allowed_positions())
    self.assertIn('c6', Board.allowed_positions())
    self.assertIn('d8', Board.allowed_positions())
