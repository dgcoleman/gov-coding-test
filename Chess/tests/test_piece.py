import unittest
from piece import Knight, Queen, Rook, Pawn, King, Bishop
from board import Board

class TestPiece(unittest.TestCase):
  
  def test_pieces_create(self):
    piece = Knight('d2')
    self.assertIsInstance(piece, Knight)
    piece = Queen('d2')
    self.assertIsInstance(piece, Queen)
    piece = Rook('d2')
    self.assertIsInstance(piece, Rook)
    piece = Pawn('d2')
    self.assertIsInstance(piece, Pawn)
    piece = King('d2')
    self.assertIsInstance(piece, King)
    piece = Bishop('d2')
    self.assertIsInstance(piece, Bishop)
    self.assertNotIsInstance(piece, King)

  def test_knight_move(self):
    orig_location = 'd2'
    piece = Knight(orig_location)
    piece.calculate_next_moves()
    # don't move
    piece.move('d2')
    self.assertEqual(piece.get_location(), orig_location)
    # move off the board
    piece.move('z99')
    self.assertEqual(piece.get_location(), orig_location)
    # move to a position not permitted
    piece.move('d4')
    self.assertEqual(piece.get_location(), orig_location)
    # move to a permitted position
    piece.move('c4')
    self.assertNotEqual(piece.get_location(), orig_location)

  def test_queen_move(self):
    orig_location = 'd2'
    piece = Queen(orig_location)
    piece.calculate_next_moves()
    # don't move
    piece.move('d2')
    self.assertEqual(piece.get_location(), orig_location)
    # move off the board
    piece.move('z99')
    self.assertEqual(piece.get_location(), orig_location)
    # move to a position not permitted
    piece.move('a3')
    self.assertEqual(piece.get_location(), orig_location)
    # move to a permitted position
    piece.move('d8')
    self.assertNotEqual(piece.get_location(), orig_location)
  
  def test_king_move(self):
    orig_location = 'd2'
    piece = King(orig_location)
    piece.calculate_next_moves()
    # don't move
    piece.move('d2')
    self.assertEqual(piece.get_location(), orig_location)
    # move off the board
    piece.move('z99')
    self.assertEqual(piece.get_location(), orig_location)
    # move more than one space
    piece.move('d4')
    self.assertEqual(piece.get_location(), orig_location)
    # move to a permitted position
    piece.move('d3')
    self.assertNotEqual(piece.get_location(), orig_location)

  def test_rook_move(self):
    orig_location = 'd2'
    piece = Rook(orig_location)
    piece.calculate_next_moves()
    # don't move
    piece.move('d2')
    self.assertEqual(piece.get_location(), orig_location)
    # move off the board
    piece.move('z99')
    self.assertEqual(piece.get_location(), orig_location)
    # try to move diagonally
    piece.move('e3')
    self.assertEqual(piece.get_location(), orig_location)
    # move to a permitted position
    piece.move('d4')
    self.assertNotEqual(piece.get_location(), orig_location)

  def test_pawn_move(self):
    orig_location = 'd2'
    piece = Pawn(orig_location)
    piece.calculate_next_moves()
    # don't move
    piece.move('d2')
    self.assertEqual(piece.get_location(), orig_location)
    # move off the board
    piece.move('z99')
    self.assertEqual(piece.get_location(), orig_location)
    # move more than one space vertically
    piece.move('d4')
    self.assertEqual(piece.get_location(), orig_location)
    # move one space horizontally
    piece.move('d3')
    self.assertEqual(piece.get_location(), orig_location)
    # move one space vertically
    piece.move('e2')
    self.assertNotEqual(piece.get_location(), orig_location)

  def test_bishop_move(self):
    orig_location = 'd2'
    piece = Bishop(orig_location)
    piece.calculate_next_moves()
    # don't move
    piece.move('d2')
    self.assertEqual(piece.get_location(), orig_location)
    # move off the board
    piece.move('z99')
    self.assertEqual(piece.get_location(), orig_location)
    # try to move horizontally
    piece.move('d3')
    self.assertEqual(piece.get_location(), orig_location)
    # try to move vertically
    piece.move('e2')
    self.assertEqual(piece.get_location(), orig_location)
    # move diagonally
    piece.move('g5')
    self.assertNotEqual(piece.get_location(), orig_location)