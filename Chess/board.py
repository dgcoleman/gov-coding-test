from itertools import chain

# this is unnecessary since the important part of this exercise
# is the flattened list of permitted x,y combinations. but the dims dict
# sort of helped me conceptualize the layout of the chessboard
dims = {
  'a' : [1,2,3,4,5,6,7,8],
  'b' : [1,2,3,4,5,6,7,8],
  'c' : [1,2,3,4,5,6,7,8],
  'd' : [1,2,3,4,5,6,7,8],
  'e' : [1,2,3,4,5,6,7,8],
  'f' : [1,2,3,4,5,6,7,8],
  'g' : [1,2,3,4,5,6,7,8],
  'h' : [1,2,3,4,5,6,7,8]
}

class Board():
  # again, sort of unnecessary at this point but, if we actually wanted to turn
  # this into a game, we would want to create an instance of the chessboard since 
  # the board would be responsible for tracking the location of all pieces. 
  def __init__(self):
    self.dims = dims

  @staticmethod
  def allowed_positions():
    allowed_positions = []
    for key in dims.keys():
      for val in dims[key]:
        allowed_positions.append(key+str(val))
    return allowed_positions