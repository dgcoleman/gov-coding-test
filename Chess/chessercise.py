import sys, argparse

from board import Board
from piece import Knight, Queen, Rook, Pawn, King, Bishop

def get_args():
  parser = argparse.ArgumentParser(description='chessercise')
  parser.add_argument("-piece", 
    choices=['QUEEN', 'ROOK', 'KNIGHT', 'KING', 'PAWN', 'BISHOP'],
    required=True, type=str, help="piece")
  parser.add_argument("-position", 
    choices=Board.allowed_positions(),
    required=True, type=str, help="piece")
  args = parser.parse_args()
  return (args.piece, args.position)

def create_piece(piece_label, position):
  if piece_label == 'KNIGHT': 
    return Knight(position)
  elif piece_label == 'QUEEN': 
    return Queen(position)
  elif piece_label == 'ROOK': 
    return Rook(position)
  elif piece_label == 'KING': 
    return King(position)
  elif piece_label == 'PAWN': 
    return Pawn(position)
  elif piece_label == 'BISHOP': 
    return Bishop(position)    
  else:
    print(f"invalid piece {piece_label}")
    sys.exit()

def do_next_move():
  print (f"next move?")
  return input() 

(piece_label, position) = get_args()
board = Board()
piece = create_piece(piece_label, position)
piece.calculate_next_moves()
print(f"possible next moves for {piece.get_name()} on space {piece.get_location()} are:")
print(f"{piece.get_next_moves()}")
moved = False
while not moved:
  moved = piece.move(do_next_move())
print(piece)  